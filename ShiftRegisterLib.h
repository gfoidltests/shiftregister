#ifndef _SHIFTREGISTER_h
#define _SHIFTREGISTER_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
//-----------------------------------------------------------------------------
class ShiftRegister
{
private:
	byte _latchPin;
	byte _clockPin;
	byte _dataPin;
	byte _output;
	static bool _pinModeSet;

public:
	ShiftRegister(const byte latchPin, const byte clockPin, const byte dataPin);

	void SetAllLow(const bool flush = true);
	void SetAllHigh(const bool flush = true);
	void SetOutput(const byte index, const bool high = true);
	void Flush(const bool msbFirst = true);

	char* getBinaryOutput();
};
//-----------------------------------------------------------------------------
#endif