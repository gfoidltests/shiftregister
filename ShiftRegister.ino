﻿#include "ShiftRegisterLib.h"

ShiftRegister shiftRegister(3, 4, 2);
bool forward = true;

void setup()
{
	Serial.begin(115200);
	Serial.println("Start");

	shiftRegister.SetAllLow();
	delay(250);
	shiftRegister.SetAllHigh();
	delay(250);
	shiftRegister.SetAllLow();
	delay(250);
}

void loop()
{
	shiftRegister.SetAllLow();
	delay(500);

	for (byte i = 0; i < 8; ++i)
	{
		shiftRegister.SetOutput(i);
		if (i > 0) shiftRegister.SetOutput(i - 1, false);
		shiftRegister.Flush(forward);
		delay(500);
	}

	shiftRegister.SetAllHigh();
	delay(500);

	shiftRegister.SetAllLow();
	delay(500);

	for (byte i = 0; i < 8; ++i)
	{
		shiftRegister.SetOutput(i);
		shiftRegister.Flush(forward);
		delay(500);
	}

	shiftRegister.SetAllLow();
	delay(500);
	shiftRegister.SetAllHigh();
	delay(500);

	shiftRegister.SetAllLow();
	delay(250);
	shiftRegister.SetAllHigh();
	delay(250);

	shiftRegister.SetAllLow();
	delay(250);

	for (byte i = 0; i < 8; ++i)
	{
		shiftRegister.SetOutput(i);
		if (i > 0) shiftRegister.SetOutput(i - 1, false);
		shiftRegister.Flush(forward);
		delay(500 - i * 60);
	}

	forward = !forward;
}