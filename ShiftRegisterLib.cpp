#define LOG_SERIAL

#include "ShiftRegisterLib.h"

bool ShiftRegister::_pinModeSet = false;
//-----------------------------------------------------------------------------
ShiftRegister::ShiftRegister(const byte latchPin, const byte clockPin, const byte dataPin)
	: _latchPin(latchPin), _clockPin(clockPin), _dataPin(dataPin)
{
	// I think it wouldn't be a problem to set it again, but I like to be on the save way
	if (!_pinModeSet)
	{
		pinMode(latchPin, OUTPUT);
		pinMode(clockPin, OUTPUT);
		pinMode(dataPin, OUTPUT);

		_pinModeSet = true;
	}
}
//-----------------------------------------------------------------------------
void ShiftRegister::SetAllLow(const bool flush)
{
	_output = 0x00;

	if (flush) this->Flush();
}
//-----------------------------------------------------------------------------
void ShiftRegister::SetAllHigh(const bool flush)
{
	_output = 0xff;

	if (flush) this->Flush();
}
//-----------------------------------------------------------------------------
void ShiftRegister::SetOutput(const byte index, const bool high)
{
	if (high)
		bitSet(_output, index);
	else
		bitClear(_output, index);
}
//-----------------------------------------------------------------------------
void ShiftRegister::Flush(const bool msbFirst)
{
	byte bitorder = msbFirst ? MSBFIRST : LSBFIRST;

	digitalWrite(_latchPin, LOW);			// doesn't have to be before shiftOut. Necessary is only the LOW->HIGH pulse for the latch
	shiftOut(_dataPin, _clockPin, bitorder, _output);
	digitalWrite(_latchPin, HIGH);

#ifdef LOG_SERIAL
	char* bits = this->getBinaryOutput();
	Serial.println(bits);	
#endif
}
//-----------------------------------------------------------------------------
char* ShiftRegister::getBinaryOutput()
{
	static char tmp[9];

	for (int i = 0; i < 8; ++i)
	{
		if (bitRead(_output, i))
			tmp[i] = '1';
		else
			tmp[i] = '0';
	}

	tmp[8] = '\0';

	return tmp;
}